import { Module } from '@nestjs/common';
import { DatabaseModule } from '@database/database.module';
import { UsersController } from '@users/controllers/users.controller';
import { UsersProvider } from '@users/providers/users.provider';
import { UsersService } from '@users/services/users.service';

@Module({
  imports: [
    DatabaseModule,
  ],
  controllers: [UsersController],
  providers: [...UsersProvider, UsersService],
  exports: [UsersService],
})
export class UsersModule {
}
