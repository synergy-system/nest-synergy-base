import { Module, Provider } from '@nestjs/common';
import { Environment } from '@env/class/environment';


const environment = process.env.NODE_ENV || 'default';

const provider: Provider = {
  provide: Environment,
  useFactory: () => import(`./environment.${environment}`).then(({ env }) => env),
};

@Module({
  providers: [provider],
  exports: [provider],
})
export class EnvinmentsModule {
}
