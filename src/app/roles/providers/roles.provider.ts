import { Connection } from 'typeorm';
import { RolEntity } from '@database/entities/rol.entity';

export const RolesProvider = [
  {
    provide: 'ROLES_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(RolEntity),
    inject: ['DATABASE_CONNECTION'],
  },
];
