import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from '@database/database.module';
import { RolesModule } from '@roles/roles.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { EnvinmentsModule } from '@env/envinments.module';

@Module({
  imports: [
    TypeOrmModule,
    DatabaseModule,
    RolesModule,
    UsersModule,
    EnvinmentsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
}
