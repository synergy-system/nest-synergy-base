import { RolesService } from '@roles/services/roles.service';
import { Body, Controller, Delete, Get, HttpCode, Logger, Param, Post, Put, Query } from '@nestjs/common';
import { RolEntity } from '@database/entities/rol.entity';

@Controller('api/v1/admin/roles')
export class RolesController {
  constructor(private readonly rolesServices: RolesService) {
  }

  @Get('')
  public async findAll(@Query('page') page: number = 0, @Query('limit') limit: number = 10) {
    limit = limit > 100 ? 100 : limit;
    return await this.rolesServices.findAll({ page, limit, route: `http://localhost:3000/api/v1/admin/roles` });
  }

  @Get(':id')
  public async findById(@Param('id') id: string) {
    return await this.rolesServices.findById(id);
  }

  @Post('')
  @HttpCode(201)
  public async create(@Body() rolEntity: RolEntity) {
    return await this.rolesServices.create(rolEntity);
  }

  @Put(':id')
  @HttpCode(201)
  public async update(@Param('id') id: string, @Body() rolEntity: Partial<RolEntity>) {
    Logger.log(`Ingresa ...`)
    return await this.rolesServices.update(id, rolEntity);
  }

  @Delete(':id')
  @HttpCode(200)
  public async delete(@Param('id') id: string) {
    return await this.rolesServices.delete(id);
  }
}
