import { Connection } from 'typeorm';
import { UserEntity } from '@database/entities/user.entity';

export const UsersProvider = [
  {
    provide: 'USERS_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(UserEntity),
    inject: ['DATABASE_CONNECTION'],
  },
];
