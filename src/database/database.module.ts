import { Module } from '@nestjs/common';
import { databaseProvider } from './providers/database.provider';
import { EnvinmentsModule } from '@env/envinments.module';

@Module({
  imports: [EnvinmentsModule],
  providers: [...databaseProvider],
  exports: [...databaseProvider],
})
export class DatabaseModule {
}
