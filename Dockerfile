FROM node:10.15-alpine
MAINTAINER Victor Hugo Cornejo
LABEL maintainer="rex2002xp@gmail.com" \
      description="Synergy API rest" \
      version="0.0.1"

RUN apk --update add git less openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

RUN npm install -g typescript
RUN npm install -g ts-node

RUN git clone https://gitlab+deploy-token-74078:X5kDkpzMyZosx32-XMg5@gitlab.com/synergy-system/nest-synergy-base.git /synergy

WORKDIR /synergy
RUN npm install

EXPOSE 3000
CMD ["ts-node","-r","tsconfig-paths/register","src/main.ts"]
