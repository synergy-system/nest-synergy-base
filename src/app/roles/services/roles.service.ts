import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { FindManyOptions, Repository } from 'typeorm';
import { RolEntity } from '@database/entities/rol.entity';
import { IPaginationOptions } from '@vendors/nestjs-typeorm-paginate/interfaces/pagination-options.interface';
import { paginate } from '@vendors/nestjs-typeorm-paginate/paginate';

@Injectable()
export class RolesService {
  constructor(
    @Inject('ROLES_REPOSITORY')
    private readonly rolesRepository: Repository<RolEntity>,
  ) {
  }

  async findAll(options: IPaginationOptions) {
    const findManyOptions: FindManyOptions = {
      loadRelationIds: true,
    };
    return await paginate<RolEntity>(this.rolesRepository, options, findManyOptions);
  }

  async findById(id: string) {
    return await this.rolesRepository.findOneOrFail(id, { loadRelationIds: true });
  }

  async create(rolEntity: Partial<RolEntity>): Promise<RolEntity> {
    return this.rolesRepository.save(rolEntity).then(record => {
      return record;
    });
  }

  async update(id: string, rolEntity: Partial<RolEntity>): Promise<RolEntity> {
    if (id === rolEntity.id) {
      const recordFound = await this.rolesRepository.findOneOrFail(rolEntity.id);
      recordFound.role = rolEntity.role;
      recordFound.description = rolEntity.description;
      return await this.rolesRepository.save(recordFound).then(record => {
        return record;
      });
    }
    throw new BadRequestException();
  }

  async delete(id: string) {
    return await this.rolesRepository.delete({ id }).then(record => {
      return record;
    });
  }
}
