import { BadRequestException, Inject, Injectable, Logger } from '@nestjs/common';
import { FindManyOptions, Repository } from 'typeorm';
import { IPaginationOptions } from '@vendors/nestjs-typeorm-paginate/interfaces/pagination-options.interface';
import { paginate } from '@vendors/nestjs-typeorm-paginate/paginate';
import { UserEntity } from '@database/entities/user.entity';

let crypto;
try {
  // tslint:disable-next-line:no-var-requires
  crypto = require('crypto');
} catch (err) {
  Logger.log('crypto support is disabled!');
}

@Injectable()
export class UsersService {
  constructor(
    @Inject('USERS_REPOSITORY')
    private readonly usersRepository: Repository<UserEntity>,
  ) {
  }

  async findAll(options: IPaginationOptions) {
    const findManyOptions: FindManyOptions = {
      loadRelationIds: true,
    };
    return await paginate<UserEntity>(this.usersRepository, options, findManyOptions);
  }

  async findById(id: string) {
    return await this.usersRepository.findOneOrFail(id, { loadRelationIds: true });
  }

  async create(userEntity: Partial<UserEntity>): Promise<UserEntity> {
    userEntity.salt = this.getSalt(50);
    userEntity.passwordHash = this.getPasswordHash('12345', userEntity.salt).passwordHash;
    return this.usersRepository.save(userEntity).then(record => {
      return record;
    });
  }

  async update(id: string, userEntity: Partial<UserEntity>): Promise<UserEntity> {
    if (id === userEntity.id) {
      const recordFound = await this.usersRepository.findOneOrFail(userEntity.id);
      if (userEntity.email) {
        recordFound.email = userEntity.email;
      }
      if (userEntity.firstName) {
        recordFound.firstName = userEntity.firstName;
      }
      if (userEntity.lastName) {
        recordFound.lastName = userEntity.lastName;
      }
      if (userEntity.status) {
        recordFound.status = userEntity.status;
      }
      if (userEntity.roles) {
        recordFound.roles = userEntity.roles;
      }
      return await this.usersRepository.save(recordFound).then(record => {
        return record;
      });
    }
    throw new BadRequestException();
  }

  async delete(id: string) {
    return await this.usersRepository.delete({ id }).then(record => {
      return record;
    });
  }

  private getSalt(length) {
    return crypto.randomBytes(Math.ceil(length / 2)).toString('hex').slice(0, length);
  }

  private getPasswordHash(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
      salt,
      passwordHash: value,
    };
  }
}
