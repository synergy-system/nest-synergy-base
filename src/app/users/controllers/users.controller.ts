import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Query } from '@nestjs/common';
import { UsersService } from '@users/services/users.service';
import { UserEntity } from '@database/entities/user.entity';

@Controller('api/v1/admin/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {
  }

  @Get('')
  public async findAll(@Query('page') page: number = 0, @Query('limit') limit: number = 10) {
    limit = limit > 100 ? 100 : limit;
    return await this.usersService.findAll({ page, limit, route: `http://localhost:3000/api/v1/admin/users` });
  }

  @Get(':id')
  public async findById(@Param('id') id: string) {
    return await this.usersService.findById(id);
  }

  @Post('')
  @HttpCode(201)
  public async create(@Body() userEntity: UserEntity) {
    return await this.usersService.create(userEntity);
  }

  @Put(':id')
  @HttpCode(201)
  public async update(@Param('id') id: string, @Body() userEntityPartial: Partial<UserEntity>) {
    return await this.usersService.update(id, userEntityPartial);
  }

  @Delete(':id')
  @HttpCode(200)
  public async delete(@Param('id') id: string) {
    return await this.usersService.delete(id);
  }
}
