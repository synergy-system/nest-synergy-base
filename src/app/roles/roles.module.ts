import { Module } from '@nestjs/common';
import { DatabaseModule } from '@database/database.module';
import { RolesService } from '@roles/services/roles.service';
import { RolesController } from '@roles/controllers/roles.controller';
import { RolesProvider } from '@roles/providers/roles.provider';

@Module({
  imports: [
    DatabaseModule,
  ],
  controllers: [RolesController],
  providers: [...RolesProvider, RolesService],
  exports: [RolesService],
})
export class RolesModule {
}
