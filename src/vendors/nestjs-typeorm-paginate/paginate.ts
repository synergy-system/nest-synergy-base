import { FindConditions, FindManyOptions, Repository } from 'typeorm';
import { Pagination } from './pagination';
import { IPaginationOptions } from './interfaces/pagination-options.interface';
import { Logger } from '@nestjs/common';

export async function paginate<T>(
  repository: Repository<T>,
  options: IPaginationOptions,
  searchOptions?: FindConditions<T> | FindManyOptions<T>,
): Promise<Pagination<T>> {
  const page =
    options.page > 0 ? options.page - 1 : options.page < 0 ? 0 : options.page;
  const limit = options.limit;
  const route = options.route;

  Logger.log(`Pagina Numero => ${page}`);

  delete options.page;
  delete options.limit;
  delete options.route;

  const [items, total] = await repository.findAndCount({
    skip: page * limit,
    take: limit,
    ...searchOptions,
  });

  const isNext = route && total / limit >= page + 1;
  const isPrevious = route && page > 0;
  const routes = {
    next: isNext ? `${route}?page=${page + 2}&limit=${limit}` : '',
    previous: isPrevious ? `${route}?page=${page}&limit=${limit}` : '',
  };

  return new Pagination(
    items,
    items.length,
    total,
    (page + 1),
    Math.ceil(total / limit),
    routes.next,
    routes.previous,
  );
}
