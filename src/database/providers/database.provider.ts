import { createConnection } from 'typeorm';
import { Logger } from '@nestjs/common';
import { Environment } from '@env/class/environment';


export const databaseProvider = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async (env: Environment) => {
      Logger.log(`env => ${JSON.stringify(env)}`);
      return createConnection({
        type: 'postgres',
        host: env.DB_HOST,
        port: env.DB_PORT,
        username: env.DB_USERNAME,
        password: env.DB_PASSWORD,
        database: env.DB_DATABASE,
        entities: [
          __dirname + '/../**/*.entity{.ts,.js}',
        ],
        synchronize: true,
        logging: true,
      });
    },
    inject: [Environment],
  },
];
